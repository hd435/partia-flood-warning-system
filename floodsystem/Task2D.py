from datafetcher import fetch_measure_levels
from station import MonitoringStation
from stationdata import build_station_list, update_water_levels
import datetime




def run():
    wanted_station = "Cam" #used Moor Lane as Cam was not working - 
    #was returning None - why i made wanted_station able to change its value
    cam_monitoring_station = None
    stations = build_station_list()
    for station in stations:
        if station.name == wanted_station:
            cam_monitoring_station = station
            break
        else:
            continue
        

    if not cam_monitoring_station:
        retur("Station Cam wasnt found")
        

    dt = 2
    dates, levels = fetch_measure_levels(
        cam_monitoring_station.measure_id, dt=datetime.timedelta(days=dt))
    print(dates,levels)
    for date, level in zip(dates, levels):
        print(date, level)


if __name__ == "__main__":
    print("*** Task 2D: CUED Part IA Flood Warning System ***")
    print(run())



