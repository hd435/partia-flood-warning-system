from plot import plot_water_levels
from datetime import datetime, timedelta
import datafetcher
import matplotlib.pyplot as plt
import datafetcher
from stationdata import build_station_list, update_water_levels
from station import MonitoringStation
from flood import stations_highest_rel_level,stations_level_over_threshold

def run():
	stations=build_station_list()
	update_water_levels(stations)
	dates=[datetime(2020,2,10),datetime(2020,2,11),datetime(2020,2,12),datetime(2020,2,13),datetime(2020,2,14),datetime(2020,2,15),datetime(2020,2,16),datetime(2020,2,17),datetime(2020,2,18),datetime(2020,2,19)]
	station_names=stations_highest_rel_level(stations,5)
	levels=None


	for i in range(len(station_names)):
		name=station_names[i][0]
		plot_water_levels(name,dates,levels)

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()