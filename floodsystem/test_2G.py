from matplotlib.dates import date2num
from analysis import polyfit, gradient
import numpy as np
from stationdata import build_station_list, update_water_levels

from datetime import datetime, timedelta   
import datafetcher
from station import MonitoringStation
from datafetcher import fetch_measure_levels
import sympy as sp
from Task2G import risk_assess



def test_2G():
    result=risk_assess(severe=0, high=0, moderate=0, low=0, p=datetime(2020,2,18))
    assert len(result)==4