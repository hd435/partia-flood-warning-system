# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None


    #Task1F
    def typical_range_consistent(self):
        #this function checks for cinsistency of low/high ranges
        #it returns false when 
        #data is unavailable
        #or high range is less than low range
        typical_range=self.typical_range
        if typical_range==None:
            return False
        elif typical_range[0]<=typical_range[1]:
            return True

    #Task2B
    def relative_water_level(self):
        #this function returns the latest water level as a fraction of the typical range
        low=None
        high=None
        if type(self.typical_range)==tuple:
            low=self.typical_range[0]
            high=self.typical_range[1]
        latest_lv=self.latest_level
        if latest_lv!=None and low!=None and high!=None:
            fraction=(latest_lv-low)/(high-low)
            return fraction



        
    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

def inconsistent_typical_range_stations(stations):
    list_5=[]
    for i in range(len(stations)):
        station=stations[i]
        if MonitoringStation.typical_range_consistent(station)==False:
            list_5.append(station.name)
    return list_5
















        
