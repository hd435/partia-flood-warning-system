from station import MonitoringStation
from stationdata import build_station_list, update_water_levels
from flood import stations_highest_rel_level,stations_level_over_threshold
from utils import sorted_by_key

def test_2C():
    """2C: returns a list of tuples, where each tuple holds (a,b). a: name of station at rist, b: relative water level
           sorted by relative water levelby descending order"""

    #build test stations
    stations=[]

    s = MonitoringStation(
    station_id='a url',
    measure_id='a url',
    label='Sidney Sussex Station',
    coord=(50,15),
    typical_range=(0,5),
    river="Chris River",
    town="Cambridgggge",)
    s.latest_level=100
    stations.append(s)
    
    s = MonitoringStation(
    station_id='a url',
    measure_id='a url',
    label='Engineering Department',
    coord=(50,15),
    typical_range=(0,3),
    river="Cam River",
    town="Cambridgggge",)
    s.latest_level=40
    stations.append(s)

    s = MonitoringStation(
    station_id='a url',
    measure_id='a url',
    label='Engineering Department',
    coord=(50,15),
    typical_range=(-1,5),
    river="Cam River",
    town="Cambridgggge",)
    s.latest_level=500
    stations.append(s)


    result=stations_highest_rel_level(stations,3)
    print(result)

    for i in range(len(result)-1):
        assert result[i][1]>=result[i+1][1]
