from utils import sorted_by_key  # noqa

def stations_level_over_threshold(stations, tol):
    #returns a list of tuples, where each tuple holds (a,b). a: name of station at rist, b: relative water level
    #sorted by relative water levelby descending order
    from station import MonitoringStation
    from utils import sorted_by_key
    list_risk=[]
    for i in range(len(stations)):
        station=stations[i]
        water_lv=MonitoringStation.relative_water_level(station)
        name=station.name
        if water_lv!=None:
            if water_lv>tol:
                s=(name,water_lv)
                list_risk.append(s)
    list_risk=sorted_by_key(list_risk,1,reverse=True)
    return list_risk

        
def stations_highest_rel_level(stations, N):
    #returns a list of the N stations with highest relative water level
    #sorted in descending order
    list_risk=stations_level_over_threshold(stations, 0)
    N_risk=[]
    for i in range(N):
        N_risk.append(list_risk[i])
    #N_risk=sorted_by_key(N_risk, 1, reverse=False)
    return N_risk[:N]