from station import MonitoringStation
from stationdata import build_station_list, update_water_levels
from flood import stations_level_over_threshold

def run():
    """2B: returns the latest water level as a fraction of the typical range"""
    stations=build_station_list()
    update_water_levels(stations)
  
    list_risk=stations_level_over_threshold(stations, 0.8)
    print(list_risk)

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()