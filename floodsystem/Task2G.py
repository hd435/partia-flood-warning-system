from matplotlib.dates import date2num
from analysis import polyfit
import numpy as np
from stationdata import build_station_list, update_water_levels

from datetime import datetime, timedelta   
import datafetcher
from station import MonitoringStation
from datafetcher import fetch_measure_levels
import sympy as sp




def risk_assess(severe=0, high=0, moderate=0, low=0, p=datetime(2020,2,18)):
    stations=build_station_list()
    now = datetime.utcnow()
    dt=now-p
    dates_conv = date2num(p)
    severe_list=["SEVERE"]
    high_list=["HIGH"]
    moderate_list=["MODERATE"]
    low_list=["LOW"]
    #for i in range(len(stations)):
    for i in range(500):
        id=stations[i].measure_id
        name=stations[i].name
        try:
            dates, levels = fetch_measure_levels(id, dt)
        except KeyError:
            pass    
        else:
            dates_conv = date2num(dates)
            
        try:
            p_coeff = polyfit(dates_conv, levels, 3)

        except IndexError:
            pass   

        else:
            a=p_coeff[0][0]
            b=p_coeff[0][1]
            c=p_coeff[0][2]
            d=p_coeff[0][3]

            x=sp.Symbol("x")
            y=sp.Symbol("y")
            f=a*(x**4)+b*(x**3)+c*(x**2)+d*(x**1)
            values=[]
            values.append(sp.limit(f,x,3))
            values.append(sp.limit(f,x,4))
            values.append(sp.limit(f,x,5))
            values.append(sp.limit(f,x,6))
            values.append(sp.limit(f,x,7))
            values.append(sp.limit(f,x,8))

            indicator=values[-1]/values[0]

            if values[-1]>0:
                if indicator>=severe:
                    severe_list.append(name)
                elif indicator>=high and indicator<severe:
                    high_list.append(name)
                elif indicator>=moderate and indicator<high:
                    moderate_list.append(name)
            elif values[-1]<0:
                low_list.append(name)
    return severe_list,high_list,moderate_list,low_list     


print(risk_assess(severe=50, high=30, moderate=20, low=10, p=datetime(2020,2,19)))