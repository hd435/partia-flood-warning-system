from plot import plot_water_levels
from datetime import datetime, timedelta
import datafetcher
import matplotlib.pyplot as plt
import datafetcher
from stationdata import build_station_list, update_water_levels
from station import MonitoringStation
from flood import stations_highest_rel_level,stations_level_over_threshold


def test_2E():


    dates=[datetime(2020, 2, 25, 18, 0),datetime(2020, 2, 24, 18, 0),datetime(2020, 2, 23, 18, 0),datetime(2020, 2, 22, 18, 0)]

    station_names=['Cam','Ledgard Bridge','Hayes Basin']
    levels=None

    for i in range(len(station_names)):
        name=station_names[i]
        assert name!=None