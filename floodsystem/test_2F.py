import datetime
from stationdata import build_station_list, update_water_levels
from datafetcher import fetch_measure_levels
from flood import stations_highest_rel_level
from plot import plot_water_level_with_fit
import matplotlib.dates
from analysis import polyfit#, gradient


def run():
    stations = build_station_list()
    update_water_levels(stations)
    highest_stations = stations_highest_rel_level(stations, 5)
    dt = 10
    id=None
    
    for s in range(len(highest_stations)):
        name=highest_stations[s][0]
        for i in range(len(stations)):
            
            if name==stations[i].name:
                id=stations[i].measure_id
                station=stations[i]
               
        dates, levels = fetch_measure_levels(id, dt=datetime.timedelta(days=dt))
     
        plot_water_level_with_fit(station, dates, levels, 3)


if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
