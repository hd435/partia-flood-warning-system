from geo import rivers_by_station_number
from geo import rivers_with_station
from stationdata import build_station_list
from utils import sorted_by_key

def test_1E():

    """Test 1E: returns a list of N rivers with the most stations"""
    stations=build_station_list()
    result = rivers_by_station_number(stations, 10)
    print(result[:10])
    for i in range(len(result)-1):
        assert result[i][1]>=result[i+1][1]