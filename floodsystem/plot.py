def plot_water_levels(station, dates, levels):
    import matplotlib.pyplot as plt
    from datetime import datetime, timedelta   
    import datafetcher
    from stationdata import build_station_list
    from station import MonitoringStation

    now = datetime.utcnow()
    stations=build_station_list()
	#find corresponding measure_id from station name
    for i in range(len(stations)):
    	one_station=stations[i]
    	if station==one_station.name:
    		id=one_station.measure_id
		
	#date and level of station for given dates and station
    for i in range(len(dates)):
        date=dates[i]
        add=date
        dt=now-add
        t, level = datafetcher.fetch_measure_levels(id, dt)
	
	#typical range of the station
    for i in range(len(stations)):
    	one_station=stations[i]
    	name2=one_station.name
    	if name2==station:
    	    typical_range=one_station.typical_range

    low=typical_range[0]
    high=typical_range[1]

    # Plot
    plt.plot(t, level)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
                    
    #plot max and min
    plt.axhline(y=low, color='r',linestyle='-')
    plt.axhline(y=high, color='r',linestyle='-')

    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    from matplotlib.dates import date2num
    from analysis import polyfit, gradient
    import matplotlib.pyplot as plt
    import numpy as np
    
    dates_conv = date2num(dates)


    poly, d0 = polyfit(dates_conv, levels, p)
    plt.plot(dates, levels, '.')
    x1 = np.linspace(dates_conv[0], dates_conv[-1], 30)
    plt.plot(x1, poly(x1 - d0))
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.tight_layout()
    plt.show()