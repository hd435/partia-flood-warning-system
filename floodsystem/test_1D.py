from geo import rivers_with_station
from geo import stations_by_river
from stationdata import build_station_list
from utils import sorted_by_key

def test_1D():

    """TEST 1D: returns a set of rivers with at least a monitoring station"""
    stations=build_station_list()
    result1 = rivers_with_station(stations)
    result2 = stations_by_river(stations)

    for i in range(len(result1)):
        for j in range(len(result1)):
            if i != i:
                assert result1[i]!=result1[j]
