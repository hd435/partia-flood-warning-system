from geo import stations_by_distance,haversine
from stationdata import build_station_list
from utils import sorted_by_key
from math import sin, cos,asin,sqrt,radians

def run():
    """1B: returns a list of stations sorted by distance"""
    stations=build_station_list()
    p= (52.2053, 0.1218)
    result = stations_by_distance(stations, p)
    print(result[:10])
   


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()