from station import MonitoringStation, inconsistent_typical_range_stations
from stationdata import build_station_list
from utils import sorted_by_key


def run():
    """1F: returns a list of stations with consistent typical range"""
    stations=build_station_list()
    result = inconsistent_typical_range_stations(stations)
    result=sorted(result)
    print(result)


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()