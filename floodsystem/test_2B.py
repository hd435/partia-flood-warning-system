from station import MonitoringStation
from stationdata import build_station_list, update_water_levels
from flood import stations_level_over_threshold

def test_2B():
    """2B: returns the latest water level as a fraction of the typical range"""

    #build test stations
    stations=[]

    s = MonitoringStation(
    station_id='a url',
    measure_id='a url',
    label='Sidney Sussex Station',
    coord=(50,15),
    typical_range=(0,5),
    river="Chris River",
    town="Cambridgggge",)
    s.latest_level=100
    stations.append(s)
    
    s = MonitoringStation(
    station_id='a url',
    measure_id='a url',
    label='Engineering Department',
    coord=(50,15),
    typical_range=(0,3),
    river="Cam River",
    town="Cambridgggge",)
    s.latest_level=40
    stations.append(s)

    s = MonitoringStation(
    station_id='a url',
    measure_id='a url',
    label='Engineering Department',
    coord=(50,15),
    typical_range=(-1,5),
    river="Cam River",
    town="Cambridgggge",)
    s.latest_level=500
    stations.append(s)


    list_risk=stations_level_over_threshold(stations, 0)
    print(list_risk)

    for i in range(len(list_risk)-1):
        assert list_risk[i][1]>=list_risk[i+1][1]
