import numpy as np
import datetime
from stationdata import build_station_list, update_water_levels
from datafetcher import fetch_measure_levels
from flood import stations_highest_rel_level
from matplotlib.dates import date2num


# dates, levels, p are equivalent to x, y, and n where n is degree of polynomial
def polyfit(dates, levels, p):
    #dates-dates[0] is the shift off time, where dates[0] is clearly the smallest date
    p_coeff = np.polyfit(dates - dates[0], levels, p)
    #p_coeff is just the coefficients of the poylnmial (doesnt include x)
    return np.poly1d(p_coeff), dates[0]
    # returns a tuple of the poylnomial object and shift of time
    

def gradient(dates, levels, p):
    p_coeff = np.polyfit(dates - dates[0], levels, p)
    p_diff = np.polyder(np.poly1d(p_coeff))
    gradient = np.polyval(np.poly1d(p_diff), 0)
    return gradient