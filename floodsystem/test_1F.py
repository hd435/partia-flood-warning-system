from station import MonitoringStation, inconsistent_typical_range_stations
from stationdata import build_station_list
from utils import sorted_by_key

def test_1F():

    """1F: returns a list of stations with consistent typical range"""
    stations=build_station_list()
    result = inconsistent_typical_range_stations(stations)
    result=sorted(result)
    for i in range(len(result)-1):
        assert result[i]<=result[i+1]