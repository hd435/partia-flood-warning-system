from geo import rivers_by_station_number
from geo import rivers_with_station
from stationdata import build_station_list
from utils import sorted_by_key


def run():
    """1E: returns a list of N rivers with the most stations"""
    stations=build_station_list()
    result = rivers_by_station_number(stations, 10)
    print(result[:10])


if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()