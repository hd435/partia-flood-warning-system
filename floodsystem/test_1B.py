from geo import stations_by_distance,haversine
from stationdata import build_station_list
from utils import sorted_by_key
from math import sin, cos,asin,sqrt,radians

def test_1B():
    """1B: returns a list of stations sorted by distance"""
    stations=build_station_list()
    p= (52.2053, 0.1218)
    result = stations_by_distance(stations, p)
    print(result[:10])    
    for i in range(len(result)-1):
         assert result[i][1]<=result[i+1][1]