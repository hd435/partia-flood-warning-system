from station import MonitoringStation
from stationdata import build_station_list, update_water_levels
from flood import stations_highest_rel_level,stations_level_over_threshold
from utils import sorted_by_key

def run():
    """2C: returns a list of tuples, where each tuple holds (a,b). a: name of station at rist, b: relative water level
           sorted by relative water levelby descending order"""
    
    stations=build_station_list()
    update_water_levels(stations)
    #result=stations_level_over_threshold(stations, 0)
    result=stations_highest_rel_level(stations,10)
    print(result)


if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run() 