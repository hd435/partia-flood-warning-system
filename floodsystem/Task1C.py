from stationdata import build_station_list
from utils import sorted_by_key
from geo import stations_within_radius,haversine
#from haversine import haversine, Unit HOW COME CANT WORK!!!!!!!1

def run():
    """1C: returns a list of stations within radius r from a coordinate x"""
    stations=build_station_list()
    x= (52.2053, 0.1218)
    r=10
    result = stations_within_radius(stations, x,r)
    result=sorted(result)
    print(result[:10])



if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()