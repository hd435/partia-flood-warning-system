# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from utils import sorted_by_key  # noqa
from math import sin, cos,asin,sqrt,radians
def haversine(p,q):
    lat1,lon1=p[0],p[1]
    lat2,lon2=q[0],q[1]
    lon1,lat1,lon2,lat2 = map(radians,[lon1,lat1,lon2,lat2])
    dlat=lat2-lat1
    dlon=lon2-lon1
    a=sin(dlat/2)**2 + cos(lat1)*cos(lat2)*sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371
    return r*c

def stations_by_distance(stations, p):
    #this function gives a list of tuples of station name 
    #and distance from coordinate p 
    
    list_1=[]
    for i in range(len(stations)):
        distance=0
        station=stations[i]
        name=station.name    #string
        coord=station.coord        #tuple, eg (1,2)
        distance=haversine(coord,p)
        indiv_station=(name,distance)
        list_1.append(indiv_station)
        list_1=sorted_by_key(list_1, 1, reverse=False)
    return list_1


def stations_within_radius(stations, centre, r):
    #this function returns a list of all stations within 
    #radius r of coordinate x
    list_2=[]
    for i in range(len(stations)):
        station=stations[i]
        name=station.name    #string
        coord=station.coord        #tuple, eg (1,2)
        distance=haversine(coord,centre)
        if distance<=r:
            list_2.append(name)
        list_2=sorted_by_key(list_2, 0, reverse=False)
    return list_2


def rivers_with_station(stations):
    #this function returns a set with the
    #names of the rivers with a monitoring station
    list_3=[]
    for i in range(len(stations)):
        station=stations[i] 
        river=station.river
        list_3.append(river)
    set_1=set(list_3)
    list_3=list(set_1)
    list_3=sorted(list_3, reverse=False)
    number=len(list_3)
    return list_3
    
    
def stations_by_river(stations):
    #this function returns a DINCTIONARY 
    #that maps river names to a list of station 
    #eg library={"river A":['station A','B'], "river B":['C','D'].......}
    dict_1 = {}
    rivers = rivers_with_station(stations)
 
    for i in range(len(rivers)):
        dict_river=rivers[i]
        station_list=[]
        for j in range(len(stations)):
            station=stations[j].name
            river=stations[j].river
            if river==dict_river:
                station_list.append(station)
        station_list=sorted(station_list)
        dict_1[dict_river]=station_list
    return dict_1

def rivers_by_station_number(stations, N):
    #this function returns a list of N rivers with the greatest number of 
    #monitoring stations, sorted by N
    #if there are more rivers with same number of stations as Nth entry
    #include them
    dict_1=stations_by_river(stations)
    list_4=[]
    for river,stations in dict_1.items():
        number=len(stations)
        s=(river, number)
        list_4.append(s)
    list_4=sorted_by_key(list_4, 1, reverse=True)
    return list_4


