from stationdata import build_station_list
from utils import sorted_by_key
from geo import stations_within_radius,haversine

def test_1C():

    """test 1C: returns a list of stations within radius r from a coordinate x"""
    stations=build_station_list()
    x= (52.2053, 0.1218)
    r=10
    result = stations_within_radius(stations, x,r)
    result=sorted(result)
    list_abc=[]
    centre=(52.2053, 0.2354)
    r=200
    for i in range(len(result)-1):
        assert result[i]<result[i+1]
    for i in range(len(stations)):
        station=stations[i]
        name=station.name    #string
        coord=station.coord        #tuple, eg (1,2)
        distance=haversine(coord,centre)
        if distance<=r:
            list_abc.append((name,distance))
    list_abc=sorted_by_key(list_abc, 1, reverse=False)
    for i in range(len(list_abc)-1):
        assert list_abc[i][1]<=list_abc[i+1][1]